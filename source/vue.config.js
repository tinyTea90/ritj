module.exports = {
    outputDir: 'docs',
    publicPath: process.env.NODE_ENV === 'production' ? '/vant-demo/' : '/',
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    red: '#f43e0d',
                    blue: '#6882fc',
                    orange: '#f08d49',
                    'search-padding': '0.33rem 0.3rem',
                    'search-input-height': '0.84rem',
                    'grid-item-content-padding': '0.29rem 0.3rem',
                    'tag-padding': '0.04rem 0.57rem',
                    'tag-font-size': '0.33rem',
                    'sidebar-width': '100%'
                }
            },
            stylus: {
                import: ['~@/global/style/varcss.styl', '~@/global/style/mixin.styl']
            }
        }
    }
};
