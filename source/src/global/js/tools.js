export default {
    formatTime (time, type = 'yyyy-mm-dd hh:mm:ss') {
        let dateTime = new Date(parseInt(time));

        let dValue = (new Date().getTime() - parseInt(time)) / 1000;

        if (dValue < 1) {
            return '刚刚';
        } else if (dValue >= 1 && dValue < 60) {
            return '1分钟前';
        } else if (dValue >= 60 && dValue < (60 * 60)) {
            return '1小时前';
        } else if (dValue >= (60 * 60) && dValue < (60 * 60 * 24)) {
            return '1天前';
        } else {
            let year = dateTime.getFullYear();
            let month = this.formatDoubleNum(dateTime.getMonth() + 1);
            let day = this.formatDoubleNum(dateTime.getDate());
            let hours = this.formatDoubleNum(dateTime.getHours());
            let minutes = this.formatDoubleNum(dateTime.getMinutes());
            let seconds = this.formatDoubleNum(dateTime.getSeconds());

            switch (type) {
                case 'yyyy-mm-dd hh:mm:ss': {
                    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
                }
                case 'yyyy-mm-dd': {
                    return `${year}-${month}-${day}`;
                }
            }
        }
    },
    formatDoubleNum (num) {
        return num < 10 ? `0${num}` : num;
    }
};
