import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        // 用户数据
        USER: {
            Phone: '13300000000'
        },
        // URL
        URL: {
            ReviewDetail: 'ReviewDetail', // ReviewDetail
            InformationTag: 'InformationTag', // 资讯中心分类列表
            AdvisoryAdvanced: 'AdvisoryAdvanced', // 咨询中心-高级咨询-免费咨询
            AdvisoryAsk: 'AdvisoryAsk', // 咨询中心-问答
            AdvisoryWeChat: 'AdvisoryWeChat', // 咨询中心-基础咨询-微信咨询
            HousesSearchList: 'HousesSearchList', // 楼盘查询条件列表
            Banner: 'Banner', // Banner列表
            Like: 'Like', // 楼盘关注、评论点赞
            ReviewCreate: 'ReviewCreate', // 楼盘评论回复
            ReviewList: 'ReviewList', // 楼盘评论列表
            HouseDetail: 'HouseDetail', // 楼盘标签详情
            AdvisoryAdvancedQuiz: 'AdvisoryAdvancedQuiz', // 咨询中心-高级咨询-免费咨询-提交问卷
            HouseDetailTopTag: 'HouseDetailTopTag', // 楼盘详细信息导航标签列表
            AdvisoryPhone: 'AdvisoryPhone', // 咨询中心-基础咨询-电话咨询
            HousesList: 'HousesList', // 楼盘查询
            HouseFavorite: 'HouseFavorite', // 关注楼盘
            HouseReviewCreate: 'HouseReviewCreate', // 楼盘评论
            HousePhoto: 'HousePhoto', // 楼盘图库图片链接
            InfomationDetail: 'InfomationDetail', // 资讯详情
            InformationList: 'InformationList' // 资讯列表
        },
        // 返码
        RESP_CODE: {
            'ok': '0000'
        },
        RESP_TEXT: {
            'netError': '网络链接超时',
            'finished': '没有更多了',
            'error': '请求失败，点击重新加载'
        },
        COLOR: {
            'theme': 'linear-gradient(to right, #9D41FA, #44A8FD)'
        },
        searchText: '',
        searchTextList: localStorage.getItem('searchText') ? localStorage.getItem('searchText').split(',') : [],
        isShowSearch: false,
        informationClassify: {
            id: '',
            name: ''
        }
    },
    mutations: {
        setUser (state, data) {
            state.USER = data;
        },
        setSearchText (state, text) {
            state.searchText = text;
        },
        setSearchList (state, text) {
            let st = localStorage.getItem('searchText');
            let textList = typeof st === 'string' ? st.split(',') : [];
            if (typeof text === 'string' && text.length > 0 && textList.indexOf(text) < 0) {
                textList.push(text);
            }
            localStorage.setItem('searchText', textList);

            state.searchTextList = textList;
        },
        clearSearchList (state) {
            localStorage.removeItem('searchText');
            state.searchTextList = [];
        },
        toggleSearch (state) {
            state.isShowSearch = !state.isShowSearch;
        },
        setInformationClassify (state, classify) {
            state.informationClassify = {
                id: classify.Id,
                name: classify.Name
            };
        }
    },
    actions: {}
});
