/**
 * author by chenming
 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import { Lazyload } from 'vant';

// 引入rem配置
import remConfig from '@/global/js/remJS';

// normalize
import 'normalize.css';

// iconfont
import '@/global/font/iconfont.css';

// tools
import tools from '@/global/js/tools';

Vue.config.productionTip = false;

remConfig();

// axios defaults
axios.defaults.baseURL = 'http://39.107.48.175:7106/api';
axios.defaults.headers['Content-Type'] = 'application/json';
axios.defaults.transformResponse = resp => {
    resp = JSON.parse(resp);
    console.log(resp);
    return resp;
};
axios.defaults.transformRequest = data => {
    console.log(data);
    return JSON.stringify(data);
};
Vue.prototype.$http = axios;

// lazyload options
Vue.use(Lazyload);

// tools
Vue.prototype.$tools = tools;

// router
router.beforeEach((to, from, next) => {
   console.log('to: %o, from: %o', to, from);
   next();
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
