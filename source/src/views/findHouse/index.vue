<template>
    <div class="container">
        <search-bar tool-btn="address" @clickSearch="fetchHouse(true)"></search-bar>
        <div class="page-wrapper">
            <van-dropdown-menu>
                <van-dropdown-item v-model="district" :options="districtList" @change="fetchHouse(true)"></van-dropdown-item>
                <van-dropdown-item title="价格" ref="price">
                    <div class="option-container">
                        <van-row>
                            <van-col span="6" class="sidebar">
                                <van-sidebar v-model="active">
                                    <van-sidebar-item title="单价"></van-sidebar-item>
                                    <van-sidebar-item title="总价"></van-sidebar-item>
                                </van-sidebar>
                            </van-col>
                            <van-col span="18" class="select-container">
                                <van-radio-group v-model="unitPrice" v-if="active === 0" @change="fetchHouse(true)">
                                    <van-cell-group>
                                        <van-cell v-for="(item, index) in unitPriceList" :key="index" :title="item.text" clickable @click="unitPrice = item.value">
                                            <van-radio slot="right-icon" :name="item.value"></van-radio>
                                        </van-cell>
                                    </van-cell-group>
                                </van-radio-group>
                                <van-radio-group v-model="totalPrice" v-if="active === 1" @change="fetchHouse(true)">
                                    <van-cell-group>
                                        <van-cell v-for="(item, index) in totalPriceList" :key="index" :title="item.text" clickable @click="totalPrice = item.value">
                                            <van-radio slot="right-icon" :name="item.value"></van-radio>
                                        </van-cell>
                                    </van-cell-group>
                                </van-radio-group>
                            </van-col>
                        </van-row>
                    </div>
                    <van-button block type="info" @click="onReset">重置</van-button>
                </van-dropdown-item>
                <van-dropdown-item v-model="subway" :options="subwayList" @change="fetchHouse(true)"></van-dropdown-item>
                <van-dropdown-item v-model="orderRule" :options="orderRuleList" @change="fetchHouse(true)"></van-dropdown-item>
            </van-dropdown-menu>
            <van-pull-refresh v-model="isLoading" @refresh="fetchHouse(true)" class="content-container">
                <van-list class="ritj-cell" v-model="houseIsLoading" @load="fetchHouse" :finished="houseIsFinished" :finished-text="this.$store.state.RESP_TEXT.finished" :error.sync="houseIsError" :error-text="this.$store.state.RESP_TEXT.error">
                    <van-cell v-for="(item, index) in houseList" :key="index" :border="false">
                        <house :data="item"></house>
                    </van-cell>
                </van-list>
            </van-pull-refresh>
        </div>
    </div>
</template>

<script type="text/ecmascript-6">
    import SearchBar from '@/components/searchBar/index';
    import { Notify, DropdownMenu, DropdownItem, PullRefresh, List, CellGroup, Cell, Button, Sidebar, SidebarItem, Row, Col, RadioGroup, Radio } from 'vant';
    import House from '@/components/house/index';

    export default {
        name: 'findHouse',
        components: {
            House,
            SearchBar,
            [DropdownMenu.name]: DropdownMenu,
            [DropdownItem.name]: DropdownItem,
            [PullRefresh.name]: PullRefresh,
            [List.name]: List,
            [CellGroup.name]: CellGroup,
            [Cell.name]: Cell,
            [Button.name]: Button,
            [Sidebar.name]: Sidebar,
            [SidebarItem.name]: SidebarItem,
            [Row.name]: Row,
            [Col.name]: Col,
            [RadioGroup.name]: RadioGroup,
            [Radio.name]: Radio
        },
        data () {
            return {
                active: 0,
                district: '',
                districtList: [],
                subway: '',
                subwayList: [],
                orderRule: '',
                orderRuleList: [],
                unitPrice: '',
                unitPriceList: [],
                totalPrice: '',
                totalPriceList: [],
                isLoading: false,
                // house data
                houseList: [],
                houseCount: 0,
                houseCurPage: 1,
                houseIsLoading: false,
                houseIsFinished: false,
                houseIsError: false
            };
        },
        created () {
            this.fetchSearchList();
        },
        methods: {
            fetchSearchList () {
                this.districtList = [];
                this.subwayList = [];
                this.orderRuleList = [];
                this.unitPriceList = [];
                this.totalPriceList = [];
                this.$http.post(this.$store.state.URL.HousesSearchList).then(resp => {
                    resp = resp.data;
                    if (resp.Code === this.$store.state.RESP_CODE.ok) {
                        resp.DistrictList.map(item => {
                            this.districtList.push({
                                text: item.Name, value: item.Id
                            });
                        });
                        resp.SubwayList.map(item => {
                            this.subwayList.push({
                                text: item.Name, value: item.Id
                            });
                        });
                        resp.OrderRuleList.map(item => {
                            this.orderRuleList.push({
                                text: item.Name, value: item.Id
                            });
                        });
                        resp.UnitPriceList.map(item => {
                            this.unitPriceList.push({
                                text: item.Name, value: item.Id
                            });
                        });
                        resp.TotalPriceList.map(item => {
                            this.totalPriceList.push({
                                text: item.Name, value: item.Id
                            });
                        });
                        this.districtList.unshift({ text: '区域', value: '' });
                        this.subwayList.unshift({ text: '地铁', value: '' });
                        this.orderRuleList.unshift({ text: '排序', value: '' });
                    }
                }).catch(() => {
                    Notify(this.$store.state.RESP_TEXT.netError);
                });
            },
            fetchHouse (isFresh = false) {
                if (isFresh) {
                    this.houseList = [];
                    this.houseCount = 0;
                    this.houseCurPage = 1;
                }
                this.$http.post(this.$store.state.URL.HousesList, {
                    Phone: this.$store.state.USER.Phone,
                    CurPage: this.houseCurPage,
                    PageSize: 10,
                    // Id: ,
                    SearchKey: this.$store.state.searchText,
                    District: this.district,
                    Subway: this.subway,
                    UnitPrice: this.unitPrice,
                    TotalPrice: this.totalPrice,
                    Order: this.orderRule
                    // IsRecommend:
                }).then(resp => {
                    this.isLoading = false;

                    this.houseIsLoading = false;

                    resp = resp.data;

                    this.houseCount = resp.Count;

                    if (resp.Code === this.$store.state.RESP_CODE.ok) {
                        this.houseCurPage++;

                        if (this.houseCount) {
                            this.houseList = this.houseList.concat(resp.List);
                        }

                        this.houseIsFinished = this.houseList.length >= this.houseCount;
                    } else {
                        Notify({ type: 'danger', message: resp.Message });
                        this.houseIsError = true;
                    }
                }).catch((err) => {
                    console.log('api/HousesList', err);

                    this.houseIsError = true;
                });
            },
            onReset () {
                this.unitPrice = '';
                this.totalPrice = '';

                this.fetchHouse(true);
            }
        }
    };
</script>

<style scoped lang="stylus" rel="stylesheet/stylus">
    .container
        height 100%
        .page-wrapper
            view-wrapper($searchHeight, $menuBarHeight)
            background #ffffff
</style>
