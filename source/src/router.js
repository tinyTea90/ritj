import Vue from 'vue';
import Router from 'vue-router';
import Root from './views/root';
import Home from './views/home/index.vue';
import Refer from './views/refer';
import advancedRefer from './views/advancedRefer';
import Reviews from './views/reviews';
import postReview from './views/postReview';
import reviewDetail from './views/reviewDetail';
import FindHouse from './views/findHouse';
import House from './views/houseView';
import houseDetail from './views/houseDetail';
import houseImages from './views/houseImages';
import My from './views/my';
import myHouse from './views/myHouse';
import myReviews from './views/myReviews';
import informationCenter from './views/informationCenter';
import informationDetail from './views/informationDetail';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'root',
            component: Root,
            redirect: '/home',
            children: [
                {
                    path: 'home',
                    name: 'Home',
                    component: Home
                },
                {
                    path: 'findHouse',
                    name: 'FindHouse',
                    component: FindHouse
                },
                {
                    path: 'refer',
                    name: 'Refer',
                    component: Refer
                },
                {
                    path: 'reviews',
                    name: 'Reviews',
                    component: Reviews
                },
                {
                    path: 'my',
                    name: 'My',
                    component: My
                }
            ]
        },
        {
            path: '/informationCenter',
            name: 'informationCenter',
            component: informationCenter
        },
        {
            path: '/informationDetail',
            name: 'informationDetail',
            component: informationDetail
        },
        {
            path: '/advancedRefer',
            name: 'advancedRefer',
            component: advancedRefer
        },
        {
            path: '/house',
            name: 'house',
            component: House
        },
        {
            path: '/houseDetail',
            name: 'houseDetail',
            component: houseDetail
        },
        {
            path: '/houseImages',
            name: 'houseImages',
            component: houseImages
        },
        {
            path: '/postReview',
            name: 'postReview',
            component: postReview
        },
        {
            path: '/reviewDetail',
            name: 'reviewDetail',
            component: reviewDetail
        },
        {
            path: '/myHouse',
            name: 'myHouse',
            component: myHouse
        },
        {
            path: '/myReviews',
            name: 'myReviews',
            component: myReviews
        }
    ]
});
